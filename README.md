# AI projects

1. Attention for Audio-visual Speech Recognition (MS.final project)
2. Document Clustering
3. Saliency model for eye fixation

Others (Master group project):

4. Low-resource language tutoring system - Virtual or Physical Social Robots Teaching a Fictional Language Through a Role-Playing Game Inspired by Game of Thrones - Human robot interaction

    If you have access to Springer: https://link.springer.com/chapter/10.1007/978-3-030-35888-4_33

    If not:
    https://www.researchgate.net/profile/Tayfun-Alpay/publication/337418392_Virtual_or_Physical_Social_Robots_Teaching_a_Fictional_Language_Through_a_Role-Playing_Game_Inspired_by_Game_of_Thrones/links/5ddba8cda6fdccdb44631f07/Virtual-or-Physical-Social-Robots-Teaching-a-Fictional-Language-Through-a-Role-Playing-Game-Inspired-by-Game-of-Thrones.pdf
    
    The article as part of Social Robotics proceeding book of the 11th International Social Robotic conference:
    https://www.google.de/books/edition/Social_Robotics/FsC_DwAAQBAJ?hl=de&gbpv=1&dq=Virtual+or+Physical%3F+Social+Robots+Teaching+a+Fictional+Language+Through+a+Role-Playing+Game+Inspired+by+Game+of+Thrones&pg=PA358&printsec=frontcover
