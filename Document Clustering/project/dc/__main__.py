
from __future__ import division

from keywordExtractor import KeywordExtractor
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from pylab import plot, show, savefig, xlim, figure, hold, ylim, legend, boxplot, setp, axes
import string
import logging
import pandas as pd
import matplotlib.pyplot as plt
import time
import math
import os
from sklearn.feature_extraction.text import TfidfVectorizer


def reduce_dimensionality(X, n_features):
    """
    Apply PCA to reduce dimension to n_features.
    :param X:
    :param n_features:
    :return:
    """
    # Initialize reduction method: PCA
    reducer = PCA(n_components=n_features)
    # Fit and transform data to n_features-dimensional space
    X=X.toarray()
    reducer.fit(X)
    X = reducer.transform(X)
    logging.debug("Reduced number of features to {0}".format(n_features))
    logging.debug("Percentage explained: %s\n" % reducer.explained_variance_ratio_.sum())
    return X



def run():
    ke = KeywordExtractor() 
    token_dict = {} 
    original_docs = {}
    one_folder_up = os.path.dirname(os.getcwd())
    os.chdir(one_folder_up)
    titles = open("data//title_list").read().split('\n')
    titles = titles[:100]
    synopses_imdb = open("data//synopses_list_imdb").read().split('\n BREAKS HERE')
    synopses_imdb = synopses_imdb[:100]
    
    #Lowering capital letters and removing punctuation
    for i in range(0,len(synopses_imdb)): 
        original_docs[titles[i]] = synopses_imdb[i]  
        synopses_imdb[i]=synopses_imdb[i].lower()
        synopses_imdb[i]=synopses_imdb[i].translate(None, string.punctuation)
        token_dict[titles[i]] = synopses_imdb[i]
    
    #sort titles alphabetically
    titles=sorted(titles)
    
    #construct the keyword-film matrix by calling keyword extraction function and tf-idf
    tfidf = TfidfVectorizer( max_df=0.8, min_df=0.2, stop_words='english',
                                 use_idf=True, tokenizer=ke.extractv2, ngram_range=(1,3))
    #start_time = time.time()
    tfs = tfidf.fit_transform(token_dict.values())
    #print("--- %s seconds for tfidf---" % (time.time() - start_time))
    keywords_list = tfidf.get_feature_names()
    keywords_list = [x.encode('UTF8') for x in keywords_list]
    film_keyword = tfs.todense()

    #Calculate the cosine similarity and euclidean distances between films using matrix
    cosine_dist = 1 - cosine_similarity(tfs)
    euclidean_dist = euclidean_distances(tfs)
    

    #start_time_k = time.time()
    #Use K-means to cluster films 
    num_clusters = 5
    km = KMeans(n_clusters=num_clusters)
    km.fit(tfs)
    #print("--- %s seconds for k-means---" % (time.time() - start_time_k))
    clusters = km.labels_.tolist() #holds the info which cluster each film belongs to
    C=km.cluster_centers_ #cluster centres

    between_distances = euclidean_distances(C) #distances between clusters using euclidean distance
    print "Distances between each cluster's centre", between_distances
#    print total_variance
    print tfs
    print keywords_list
    print cosine_dist
    print euclidean_dist
    print clusters

    logging.debug("Preparing visualization of DataFrame")
    # Reduce dimensionality to 2 features for visualization purposes
    pca_visualization = reduce_dimensionality(tfs, n_features=2)
    xs, ys= pca_visualization[:, 0], pca_visualization[:, 1]
    #set up colors per clusters using a dict
    cluster_colors = {0: '#1b9e77', 1: '#d95f02', 2: '#7570b3', 3: '#e7298a', 4: '#66a61e'}

    #set up cluster names using a dict
    cluster_names = {0: 'Cluster 1',
                 1: 'Cluster 2',
                 2: 'Cluster 3',
                 3: 'Cluster 4',
                 4: 'Cluster 5'}


    df = pd.DataFrame(dict(x=xs, y=ys, label=clusters, title=titles))
    groups = df.groupby('label')
    
    #Constructing the table for the features of each cluster
    cluster_one=df[df.label==0]    
    cluster_two=df[df.label==1]
    cluster_three=df[df.label==2]
    cluster_four=df[df.label==3]
    cluster_five=df[df.label==4]    
    cluster_onetable=cluster_one.describe()
    cluster_twotable=cluster_two.describe()
    cluster_threetable=cluster_three.describe()
    cluster_fourtable=cluster_four.describe()
    cluster_fivetable=cluster_five.describe()
    cluster_table = pd.DataFrame(dict(cluster1x=cluster_onetable.loc[:,"x"], cluster1y=cluster_onetable.loc[:,"y"],
                                      cluster2x=cluster_twotable.loc[:,"x"], cluster2y=cluster_twotable.loc[:,"y"], 
                                      cluster3x=cluster_threetable.loc[:,"x"], cluster3y=cluster_threetable.loc[:,"y"],
                                      cluster4x=cluster_fourtable.loc[:,"x"], cluster4y=cluster_fourtable.loc[:,"y"],
                                      cluster5x=cluster_fivetable.loc[:,"x"], cluster5y=cluster_fivetable.loc[:,"y"]))
    print cluster_table                                  
    
    #Calculate global mean of all films   
    global_mean = [0 for x in range(2)]
    global_mean[0]=(cluster_table.loc["mean","cluster1x"]+cluster_table.loc["mean","cluster2x"]+cluster_table.loc["mean","cluster3x"]+cluster_table.loc["mean","cluster4x"]+cluster_table.loc["mean","cluster5x"])/5
    global_mean[1]=(cluster_table.loc["mean","cluster1y"]+cluster_table.loc["mean","cluster2y"]+cluster_table.loc["mean","cluster3y"]+cluster_table.loc["mean","cluster4y"]+cluster_table.loc["mean","cluster5y"])/5   
    
    #take a random sample of 10 from 100 films
    df_sample=df.sample(n=10)
    #calculate total square of distances to the global mean
    total_SS=0
    for i in range(0,len(df_sample)):
        ss = 0
        for j in range(0, 2):
            ss = (df_sample.iloc[i, j+2]-global_mean[j])**2 
        total_SS = math.sqrt(ss) + total_SS    
        
    # calculate total square of clusters' centres' distances to the global mean
    between_SS = 0
    for i in range(0, num_clusters):
        ss = 0
        for j in range(0, 2):
            ss = ss + (cluster_table.iloc[1,i+j] - global_mean[j]) ** 2
        between_SS = math.sqrt(ss) + between_SS
        
    #calculate total variance
    total_variance = between_SS / total_SS
    print "Total variance of clusters: ", total_variance
    
    #form the boxplot
    # function for setting the colors of the box plots pairs
    def setBoxColors(bp):
        setp(bp['boxes'][0], color='blue')
        setp(bp['caps'][0], color='blue')
        setp(bp['caps'][1], color='blue')
        setp(bp['whiskers'][0], color='blue')
        setp(bp['whiskers'][1], color='blue')
        setp(bp['medians'][0], color='blue')

        setp(bp['boxes'][1], color='red')
        setp(bp['caps'][2], color='red')
        setp(bp['caps'][3], color='red')
        setp(bp['whiskers'][2], color='red')
        setp(bp['whiskers'][3], color='red')
        setp(bp['medians'][1], color='red')    
    
    
    A = [cluster_one.loc[:,"x"].tolist(),  cluster_one.loc[:,"y"].tolist()]
    B = [cluster_two.loc[:,"x"].tolist(), cluster_two.loc[:,"y"].tolist()]
    C = [cluster_three.loc[:,"x"].tolist(), cluster_three.loc[:,"y"].tolist()]
    D = [cluster_four.loc[:,"x"].tolist(), cluster_four.loc[:,"y"].tolist()]
    E = [cluster_five.loc[:,"x"].tolist(), cluster_five.loc[:,"y"].tolist()]
    
    #set up box plot
    fig = figure()
    ax = axes()
    hold(True)

    # first boxplot pair
    bp = boxplot(A, positions = [0.5, 1.5], widths = 0.6)
    setBoxColors(bp)

    # second boxplot pair
    bp = boxplot(B, positions = [3.5, 4.5], widths = 0.6)
    setBoxColors(bp)

    # third boxplot pair
    bp = boxplot(C, positions = [6.5, 7.5], widths = 0.6)
    setBoxColors(bp)
    
    # fourth boxplot pair
    bp = boxplot(D, positions = [9.5, 10.5], widths = 0.6)
    setBoxColors(bp)
    
    # fifth boxplot pair
    bp = boxplot(E, positions = [12.5, 13.5], widths = 0.6)
    setBoxColors(bp)

    # set axes limits and labels
    xlim(-1,1)
    ylim(-1,1)
    ax.set_xticklabels(['Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster 5'])
    ax.set_xticks([1, 4, 7, 10, 13])
    
    # draw temporary red and blue lines and use them to create a legend
    hB, = plot([1,1],'b-')
    hR, = plot([1,1],'r-')
    legend((hB, hR),('1st Dimension', '2nd Dimension'))
    hB.set_visible(False)
    hR.set_visible(False)
    # set up plot
    fig, ax = plt.subplots(figsize=(17, 9)) # set size
    ax.margins(0.05) # Optional, just adds 5% padding to the autoscaling

    #show results of clustering in 2D view
    for name, group in groups:
        ax.plot(group.x, group.y, marker='o', linestyle='', ms=12,
                label=cluster_names[name], color=cluster_colors[name],
                mec='none')
        ax.set_aspect('auto')
        ax.tick_params(\
            axis= 'x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom='off',      # ticks along the bottom edge are off
            top='off',         # ticks along the top edge are off
            labelbottom='off')
        ax.tick_params(\
            axis= 'y',         # changes apply to the y-axis
            which='both',      # both major and minor ticks are affected
            left='off',      # ticks along the bottom edge are off
            top='off',         # ticks along the top edge are off
            labelleft='off')


    ax.legend(numpoints=1)  #show legend with only 1 point

    for i in range(len(df)):
        ax.text(df.ix[i]['x'], df.ix[i]['y'], df.ix[i]['title'], size=8)
    plt.show()
if __name__ == "__main__":
    run()

