from __future__ import division
from nltk.stem.snowball import SnowballStemmer
from nltk.stem import WordNetLemmatizer
import re
import nltk

class KeywordExtractor:
    def __init__(self):
        self.stopwords = nltk.corpus.stopwords.words('english')
        self.stemmer = SnowballStemmer("english")
        self.lemmatizer = WordNetLemmatizer()
        #self.pos = nltk.pos_tag
    
    #keyword extraction function which takes the stem of each keyword    
    def extract(self, text):
        tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
        filtered_tokens = []
        for token in tokens:
            if re.search('[a-zA-Z]', token):
                filtered_tokens.append(token)
        filtered_tokens = [w for w in filtered_tokens if not w in self.stopwords]        
        stems = [self.stemmer.stem(t) for t in filtered_tokens]
        stems = [stem for stem in stems if len(stem)>1 ]
        return stems    
        #return filtered_tokens
        
    #keyword extraction function which uses a lemmatizer to extract keywords     
    def extractv2(self, text):
        tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
        filtered_tokens = []
        for token in tokens:
            if re.search('[a-zA-Z]', token):
                filtered_tokens.append(token)
        filtered_tokens = [w for w in filtered_tokens if not w in self.stopwords] 
        lems = [self.lemmatizer.lemmatize(t) for t in filtered_tokens]   
        return lems   
        
    #keyword extraction function which uses a lemmatizer to extract keywords(only nouns)    
    def extractwithLemmatizer(self, text):
        tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
        tagged = nltk.pos_tag(tokens)
        nouns = [word for word,pos in tagged \
                if (pos == 'NN' or pos == 'NNP' or pos == 'NNS' or pos == 'NNPS')]
        filtered_tokens = []
        for noun in nouns:
            if re.search('[a-zA-Z]', noun):
                filtered_tokens.append(noun)
        filtered_tokens = [w for w in filtered_tokens if not w in self.stopwords] 
        lems = [self.lemmatizer.lemmatize(t) for t in filtered_tokens]       
        return lems        
    
    #keyword extraction function which uses a stemmer to extract keywords(only nouns)    
    def extractwithStemmer(self, text):
        tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
        tagged = nltk.pos_tag(tokens)
        nouns = [word for word,pos in tagged \
                if (pos == 'NN' or pos == 'NNP' or pos == 'NNS' or pos == 'NNPS')]
        filtered_tokens = []
        for noun in nouns:
            if re.search('[a-zA-Z]', noun):
                filtered_tokens.append(noun)
        filtered_tokens = [w for w in filtered_tokens if not w in self.stopwords] 
        stems = [self.stemmer.stem(t) for t in filtered_tokens]       
        stems = [stem for stem in stems if len(stem)>1 ]
        return stems            