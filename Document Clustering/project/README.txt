Document Clustering Project:
An NLP project for the clustering of top 100 films according to their synopses from IMDB.

The project is splitted in 2 self made scripts. Run it from __main__.py.
Following libraries are required to run the project:
-Pandas
-Pylab
-Sklearn
-Matplotlib
-NLTK

Film titles and film synopses can be found under the 'data' folder.
