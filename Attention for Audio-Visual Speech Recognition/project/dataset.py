#encoding: utf-8

#Adapted from github.com/mpc001/end-to-end-lipreading

import os
import cv2
import glob
import random
import scipy.io
import numpy as np
import re

def load_audio_file(filename):
    return np.load(filename)['data']


def load_video_file(filename):
    cap = np.load(filename)['data']
    arrays = np.stack([cv2.cvtColor(cap[_], cv2.COLOR_RGB2GRAY) for _ in range(29)], axis=0)
    arrays = arrays / 255.
    return arrays


class MyDataset():
    def __init__(self, folds, audio_path, video_path):
        self.folds = folds
        self.audio_path = audio_path
        self.video_path = video_path
        self.clean = 1 / 7.
        with open('../label_sorted.txt') as myfile:
            self.data_dir = myfile.read().splitlines()
        self.filenames = glob.glob(os.path.join(self.audio_path, '*', self.folds, '*.npz'))
        print("file", self.filenames)
        self.list = {}
        for i, x in enumerate(self.filenames):
            print('x value', x)
            target = x.split('/')[-3]
            for j, elem in enumerate(self.data_dir):
                if elem == target:
                    self.list[i] = [x]
                    self.list[i].append(j)
      

    def normalisation(self, inputs):
        inputs_std = np.std(inputs)
        if inputs_std == 0.:
            inputs_std = 1.
        return (inputs - np.mean(inputs))/inputs_std

    def __getitem__(self, idx):
        video_inputs = load_video_file(os.path.join(self.video_path,
                                                    self.list[idx][0].split('/')[-3],
                                                    self.list[idx][0].split('/')[-2],
                                                    self.list[idx][0].split('/')[-1][:-4]+'.npz'))
        result = re.findall(r'[^\\/]+|[\\/]', self.list[idx][0]) 
        f = ''.join(result[10])
        path_f = ''.join(result[0:8]) 
        # for clean audio noise_prop and temp
        noise_prop = (1-self.clean)/3.
        temp = random.random()
     
        ## ALL THE CONDTIONS ARE FOR CLEAN AUDIO DATA AUGMENTATION
        if self.folds == 'train':
            if temp < noise_prop:
                self.list[idx][0] = path_f+'NoisyAudio/-10dB/'+f
            elif temp < 2 * noise_prop:
                self.list[idx][0] = path_f+'NoisyAudio/-20dB/'+f
            elif temp < 3 * noise_prop:
                self.list[idx][0] = path_f+'NoisyAudio/-30dB/'+f
          #  else:
          #  self.list[idx][0] = self.list[idx][0]
       #     print('list idx', self.list[idx][0])
        elif self.folds == 'val' or self.folds == 'test':
            self.list[idx][0] = self.list[idx][0]
        audio_inputs = load_audio_file(self.list[idx][0])
        audio_inputs = self.normalisation(audio_inputs)
        labels = self.list[idx][1]
        return audio_inputs, video_inputs, labels

    def __len__(self):
        return len(self.filenames)
