import math
import numpy as np
import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable

class GRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, num_classes, every_frame=True):
        super(GRU, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.every_frame = every_frame

        self.gru = nn.GRU(input_size, hidden_size, num_layers, batch_first=True, bidirectional=True)
        self.fc = nn.Linear(hidden_size*2, num_classes)

    def forward(self, x):
        print('x value',x.size())
        h0 = Variable(torch.zeros(self.num_layers*2, x.size(0), self.hidden_size))
        print(h0.size())
        out, _ = self.gru(x, h0)
        print('out', out.size())
        return out

class Attention(nn.Module):

    def __init__(self,hiddenDim):
        super(Attention, self).__init__()
        self.hiddenDim = hiddenDim
        self.nLayers = 2
        self.nClasses = 3
        self.every_frame = True
        self.dense = nn.Sequential(
                nn.Linear(self.hiddenDim+self.hiddenDim, self.hiddenDim),
             ## nn.Tanh(),
                nn.ReLU(), 
                nn.Linear(self.hiddenDim, 1) 
                ) 
        self.gru_audio = GRU(self.hiddenDim, self.hiddenDim, self.nLayers, self.nClasses, self.every_frame)
        

        self.gru_video = GRU(self.hiddenDim, self.hiddenDim, self.nLayers, self.nClasses, self.every_frame)


       #Parameters
       # ----------
       # values audio : 3-D torch Tensor 
       # audio output torch.Size([5, 29, 1024])
       # values video : 3-D torch Tensor #
       # video output torch.Size([5, 29, 1024]) 
       # (Batch_size, sequence_length, number_of_features)
        
       # Returns
     #   -------
       # context : 3-D torch Tensor #output of attention
 
    def forward(self, inputs_a, inputs_v):
        
        print('HERE ATTENTION')
        scores_a = self.dense(inputs_a).squeeze(2)
      #  print('scores_audio', scores_a)
        print('scores_audio size', scores_a.size())
        scores_v = self.dense(inputs_v).squeeze(2)
      #  print('scores_vide', scores_v)
        print('scores_video size', scores_v.size())
        inputs=torch.cat([scores_a, scores_v], dim=0)
        print('inputs merge size',inputs.size())
        alphas = F.softmax(inputs).unsqueeze(1)
        print('alphas', alphas.size())
        val = len(alphas)//2
        print('alphas len', len(alphas))
        context_split=torch.split(alphas,val,dim=0)
        print('context split', context_split[0].size())
        context_audio=context_split[0].bmm(inputs_a)
        print('context audio', context_audio.size())
        context_video=context_split[1].bmm(inputs_v)
        print('context video', context_video.size())
       # FOR THE NEW ATTENTION GRU FOR AUDIO AND VIDEO 
        context_audio_new = context_audio.view(context_audio.size(0)*2,context_audio.size(1),-1)
        print('context audio new', context_audio_new.size())
        x_audio = self.gru_audio(context_audio_new)
        print('gru_audio', x_audio.size())
        context_video_new = context_video.view(context_video.size(0)*2,context_video.size(1),-1)
        x_video = self.gru_video(context_video_new)
        print('gru_video', x_video.size())
        return x_audio, x_video


#1024
def attention(mode, hiddenDim=512):
#def Attention(mode, inputDim=256, hiddenDim=512, nClasses=3, frameLen=25, every_frame=True):
    if mode == 'backendGRU' or mode == 'finetuneGRU':
        model = Attention(hiddenDim=hiddenDim)
    print('\n'+mode+' model attention has been built')    
     #   model = Attention(mode=mode,hiddenDim=hiddenDim)
    return model

