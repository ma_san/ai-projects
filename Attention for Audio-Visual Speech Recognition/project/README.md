# Attention for Audio-Visual Speech Recognition
	Attention Mechanism for Dynamic Weighting between Audio-Visual Inputs for Speech Recognition
    
    In this thesis, a dynamic weighting between audio inputs and visual inputs is proposed. 
    This weighting mechanism, known as attention, computes audio and video streams' weights based on their reliability under noisy and clean conditions. 
    Several methods have been proposed before to improve recognition performance, fusing audio and visual modalities. 
    The technique used in this thesis is a modality attention mechanism that relies on the features of each modality, either audio or video, to perform word recognition. 
    Experimental results indicate that the proposed model could enhance audio-visual speech recognition under noise conditions for both streams. 
    However, further research would be necessary to refine the mechanism and execute experiments on a larger scale.
    Two types of attention implemented : FNN and LSTM Network
    Datasets: BBC LWR Lipreading dataset and DEMAND Noise Dataset
    Pytorch implementation




