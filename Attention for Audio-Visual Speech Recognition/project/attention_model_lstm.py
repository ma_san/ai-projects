import math
import numpy as np
import torch
import matplotlib.pyplot as plt
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable

class GRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, num_classes, every_frame=True):
        super(GRU, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.every_frame = every_frame

        self.gru = nn.GRU(input_size, hidden_size, num_layers, batch_first=True, bidirectional=True)
        self.fc = nn.Linear(hidden_size*2, num_classes)

    def forward(self, x):
        print('x value',x.size())
        h0 = Variable(torch.zeros(self.num_layers*2, x.size(0), self.hidden_size))
        print(h0.size())
        out, _ = self.gru(x, h0)
        print('out', out.size())
        return out

class Attention(nn.Module):

  
     def __init__(self, inputDim=2048, hiddenDim=512, nLayers=1, outputDim=1):#, drop_prob=0.5): 
        super(Attention, self).__init__()
     
        self.hiddenDim = hiddenDim
        self.nLayers = nLayers
        self.nClasses = 3
        self.every_frame = True
        # (batch_dim, seq_dim, feature_dim)
        self.lstm = nn.LSTM(inputDim, hiddenDim, nLayers, batch_first=True) 
      
        self.fc = nn.Linear(hiddenDim, outputDim)

        self.gru_audio = GRU(self.hiddenDim*2, self.hiddenDim, self.nLayers, self.nClasses, self.every_frame)
        
        #self.hiddenDim*2
        self.gru_video = GRU(self.hiddenDim*2, self.hiddenDim, self.nLayers, self.nClasses, self.every_frame)

       #Parameters
       # ----------
       # values audio : 3-D torch Tensor 
       # audio output torch.Size([5, 29, 1024])
       # values video : 3-D torch Tensor #
       # video output torch.Size([5, 29, 1024]) 
       # (Batch_size, sequence_length, number_of_features)
        
       # Returns
     #   -------
       # context : 3-D torch Tensor #output of attention
 
     def forward(self, inputs_a, inputs_v):
        
        print('HERE ATTENTION')
        print('inputs a', inputs_a.size())
        inputs_a_new = torch.unsqueeze(inputs_a, 1)
        print('inputs_a_new', inputs_a_new.size())
        inputs_v_new = torch.unsqueeze(inputs_v, 1)
        print('inputs_v_new',inputs_v_new.size())
        inputs=torch.cat([inputs_a, inputs_v], dim=2)
      # Initialize hidden state
        h0 = Variable((torch.zeros(self.nLayers, inputs.size(0), self.hiddenDim)),requires_grad=True)
      # Initialize cell state
        c0 = Variable((torch.zeros(self.nLayers, inputs.size(0), self.hiddenDim)),requires_grad=True) 
        #detach to truncate backpropagation
        #lstm for attention
        lstm, (hn,cn) = self.lstm(inputs, (h0.detach(),c0.detach()))
        print('lstm size', lstm.size())
        #scoring 
        scores = self.fc(lstm)
        print('scores size', scores.size())
        scores = scores.squeeze(2)
        print('scores  size squeezing', scores.size())
        #normalization
        alphas = F.softmax(scores,dim=1).unsqueeze(1)
        print('alphas', alphas.size())
        val = len(alphas)//2
        print('alphas len', len(alphas))

        # ALPHAS = WEIGHTS FOR AUDIO AND VIDEO
        context = alphas.bmm(inputs)
        print('context', context.size())
        
        #print values
        context_split = torch.split(alphas, val, dim=0)
        context_prev = context_split[0]
        print('context split audio', context_split[0].size)
        print('audio values', context_split[0])
       
        X = context_prev.view(context_prev.size(0)*context_prev.size(1)*context_prev.size(2)) 
        print('audio size', X.size())
        datax=X.cpu()
        data=datax.detach().numpy()
        print('data audio',data.shape)
        box_plot_data=[data]
        plt.boxplot(box_plot_data,patch_artist=True,labels=['exp1'])
        plt.savefig('fig.png')
        plt.close()

        return context

#1024
def attention(mode, inputDim=2048, hiddenDim=512, nLayers=1, outputDim=1):#, drop_prob=0.5 ):

    if mode == 'backendGRU' or mode == 'finetuneGRU':
        model = Attention(inputDim=inputDim, hiddenDim=hiddenDim, nLayers=nLayers, outputDim=outputDim )#, drop_prob=drop_prob)
    print('\n'+mode+' model attention has been built')    
     #   model = Attention(mode=mode,hiddenDim=hiddenDim)
    return model

