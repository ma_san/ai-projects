#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os
import glob


VIDEOS_PATH = ''
VIDEOS_EXTENSION = '.wav'  # for example
AUDIO_EXT = 'mp4'

AUDIO_PATH = ''



EXTRACT_VIDEO_COMMAND = ('ffmpeg -i "{from_video_path}" '
                         '-f {audio_ext} -ab 64000 '
                         '-ar 16000 '
                         '-vn "{to_audio_path}"')
filenames = glob.glob(os.path.join(VIDEOS_PATH, '*', '*', '*.wav'))

for f in filenames:    
 
    actual_filename = f[:-4] # delete the extension
    print(actual_filename)
 
    output_path = os.path.join(AUDIO_PATH,f.split('/')[-3], f.split('/')[-2], f.split('/')[-1][:-4]+'.mp4')
    #audio_file_name = '{}.{}'.format(f, AUDIO_EXT)
    print(output_path)
    #print(audio_file_name)
    command = EXTRACT_VIDEO_COMMAND.format(
        from_video_path=f, audio_ext=AUDIO_EXT, to_audio_path=output_path,
    )
    os.system(command)
 




