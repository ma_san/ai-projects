#!/usr/bin/env python
# coding: utf-8

# In[2]:


# -*- coding: utf-8 -*-

#Adapted from github.com/mpc001/end-to-end-lipreading

"""
Extract non-mouth regions (new ROI) and transforms the video format to npz

"""
from PIL import Image
import os
import cv2
import glob
import numpy as np
import re
import torch
from pathlib import Path



def extract_opencv(filename):
    video = []
    cap = cv2.VideoCapture(filename)

    while(cap.isOpened()):
        ret, frame = cap.read() # BGR
        if ret:
            video.append(frame)
        else:
            break
    cap.release()
    video = np.array(video)
    #result = video[..., ::-1] - np.zeros_like(video)
    return video[...,::-1]
   

def extract_video(filename):
    video = []
    cap = cv2.VideoCapture(filename)
    while(cap.isOpened()):
        ret, frame = cap.read() # BGR
        if ret:
           # cv2.imshow('Frame',frame)
            video.append(frame)
        else:
            break
    cap.release()
    video_real = video
    return video_real





basedir_to_save = ''

basedir = ''

dataset = ['test','train','val']

for data in dataset:
    if (data=='test'):
        x=os.path.join(basedir, 'test', '*.mp4')
        print("test yes")
        quantity=50 
    if (data=='train'):
        x=os.path.join(basedir, 'train', '*.mp4')
        print("train yes")
        quantity=1000 
    if (data=='val'):
        x=os.path.join(basedir, 'val', '*.mp4')
        print("val yes")
        quantity=50 
 
    print(x)
    filenames = glob.glob(x)

    count =0
    for filename in filenames:
        videos = extract_video(filename)
         #non-mouth region
        data = extract_opencv(filename)[:, 215:311, 179:275]
        # real mouth region 
        #data = extract_opencv(filename)[:, 115:211, 79:175]
        #reshape non-mouth region
        data_new=np.resize(data, (29,96,96,3))

        
        path_to_save = os.path.join(basedir_to_save,
                                    filename.split('/')[-3],
                                    filename.split('/')[-2],
                                    filename.split('/')[-1][:-4]+'.npz')
        if not os.path.exists(os.path.dirname(path_to_save)):
            try:
                os.makedirs(os.path.dirname(path_to_save))
            except OSError as exc:
                if exc.errno != errno.EEXIST:
                    raise
        np.savez( path_to_save, data=data_new)

