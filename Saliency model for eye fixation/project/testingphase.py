#!/usr/bin/env python


import tensorflow as tf
import math
import numpy as np
import os
import h5py
import matplotlib.pyplot as plt
import scipy
import imageio
from PIL import Image
from scipy import ndimage
from tensorflow.python.framework import ops
from skimage import data, io
from scipy.misc.pilutil import  imread, imresize
import six
from PIL import Image

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


def load_test_data():
    test_img_directory = '../project/data/test/images'

    #input images
    test_imgs = np.zeros((400, 180, 320, 3), dtype=np.uint8)
    #input fixation map
    #train_fixations = np.zeros((400, 180, 320, 1), dtype=np.uint8)

    for i in range(1601, 2001):
        img_file = os.path.join(test_img_directory, '{:04d}.jpg'.format(i))
       # fixation_file = os.path.join(training_fixation_directory, '{:04d}.jpg'.format(i))
        test_imgs[i-1601] = imageio.imread(img_file)
       # fixation = imageio.imread(fixation_file)
       # train_fixations[i-1] = np.expand_dims(fixation, -1) # adds singleton dimension so fixation size is (180,320,1)
    
    return test_imgs



def data_generator_test(imgs):
    while True: # produce new epochs forever
        # Shuffle the data for this epoch
        idx = np.arange(imgs.shape[0])
        output_type=tf.float32, output_shapes=(400,180,320,1)
        imgs = imgs[idx]
        for i in range(imgs.shape[0]):
            yield imgs[i],output[i]


def get_batch_from_generator_test(gen, batchsize):
    batch_imgs = []
    batch_fixations = []
    output_type=tf.float32, output_shapes=(400,180,320,1)
    for i in range(batchsize):
        img = next(gen)
        batch_imgs.append(img)
      #  batch_fixations.append(target)
    return np.array(batch_imgs), None
### loading

test_ims=load_test_data()

with tf.Session() as sess:
    load_model=tf.train.import_meta_graph("./my-model-9.meta")	
    load_model.restore(sess,tf.train.latest_checkpoint('./'))
    model = tf.estimator.Estimator(model_fn=load_model, model_dir='/.my-model-9',params={})	
    print(model)
    #new_model.restore(sess,"./my-model")	
    test_batch_size = 400
    num_test_batches = test_imgs.shape[0]/test_batch_size
    test_losses = []
    #test_accs = []   
        
    gen = data_generator_test(test_imgs)
    
    for test_batch in range(num_test_batches):
        test_imgs, test_fixations = get_batch_from_generator(gen, batch_size)
        start_idx = test_batch * test_batch_size
        stop_idx = start_idx + test_batch_size
        test_idx = np.arange(start_idx, stop_idx)
    #    input_test = tf.estimator.inputs.numpy_input_fn(x={'images': test_imgs},    	y=np.array(sess.run(fixations), batch_size=test_batch_size, shuffle=False)
#predicted_sal=tf.get_variable(initializer=predicted_saliency,shape=[400,180,320,3],name="predicted_sal")
#ims_holder,targs_holder=create_placeholder(h_size,w_size,c_ninp,c_ntarg)
      #  evaluate=model.evaluate(input_test)

        test_loss,iraw = sess.run([loss, image_raw[0]], feed_dict={ims_holder: test_imgs[test_idx]})
        print('Test batch {:d} done: batch loss {:f}, batch accuracy {:f}'.format(test_batch, test_loss))
        test_losses.append(test_loss)
        test_accs.append(test_accuracy)
    print('Test loss: {:f}'.format(np.average(test_losses)))

