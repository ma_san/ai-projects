#!/usr/bin/env python


import tensorflow as tf
import math
import numpy as np
import os
import h5py
import matplotlib.pyplot as plt
import scipy
import imageio
from PIL import Image
from scipy import ndimage
from tensorflow.python.framework import ops
from skimage import data, io
from scipy.misc.pilutil import  imread, imresize
import six
from PIL import Image

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

#unzip "data/data_fixation.zip" before

def load_train_data():
    training_img_directory = '../project/data/train/images'
    training_fixation_directory = '../project/data/train/fixations'
    #input images
    train_imgs = np.zeros((1200, 180, 320, 3), dtype=np.uint8)
    #input fixation map
    train_fixations = np.zeros((1200, 180, 320, 1), dtype=np.uint8)
    #real end 1201
    for i in range(1, 1200):
        img_file = os.path.join(training_img_directory, '{:04d}.jpg'.format(0000+i))
        fixation_file = os.path.join(training_fixation_directory, '{:04d}.jpg'.format(0000+i))
        train_imgs[i-1] = imageio.imread(img_file)
        fixation = imageio.imread(fixation_file)
        train_fixations[i-1] = np.expand_dims(fixation, -1) # adds singleton dimension so fixation size is (180,320,1)

    return train_imgs, train_fixations


def data_generator(imgs, targets):
    while True: # produce new epochs forever
        # Shuffle the data for this epoch
        idx = np.arange(imgs.shape[0])
        np.random.shuffle(idx)

        imgs = imgs[idx]
        targets = targets[idx]
        for i in range(imgs.shape[0]):
            yield imgs[i], targets[i]


def get_batch_from_generator(gen, batchsize):
    batch_imgs = []
    batch_fixations = []
    for i in range(batchsize):
        img, target = next(gen)
        batch_imgs.append(img)
        batch_fixations.append(target)
    return np.array(batch_imgs), np.array(batch_fixations)


num_batches = 10 # 
batch_size = 32 # 


def create_placeholder(h_size,w_size,c_ninp,c_ntarg):
    images_holder = tf.placeholder(tf.float32, [None, h_size, w_size, c_ninp])
    #target float 32
    target_holder = tf.placeholder(tf.float32, [None,h_size,w_size, c_ntarg])
    return images_holder, target_holder


h_size=180
w_size=320
c_ninp=3
c_ntarg=1
image_raw=[]

### loading

train_imgs, train_fixations = load_train_data()
ims_holder,targs_holder=create_placeholder(h_size,w_size,c_ninp,c_ntarg)

vgg_weight_file = '../project/vgg16-conv-weights.npz'
weights = np.load(vgg_weight_file)

with tf.name_scope('preprocess') as scope:
    images = tf.image.convert_image_dtype(ims_holder, tf.float32, saturate=False) * 255.0
    mean = tf.constant([123.68, 116.779, 103.939], dtype=tf.float32, shape=[1, 1, 1, 3], name='img_mean')
    imgs_normalized = images - mean

    fixations = tf.image.convert_image_dtype(targs_holder, tf.float32, saturate=False) * 255.0
    mean = tf.constant([123.68, 116.779, 103.939], dtype=tf.float32, shape=[1, 1, 1, 3], name='fixation_mean')
    fixations_preprocess = fixations - mean
    fixations_normalized=tf.image.rgb_to_grayscale(fixations_preprocess)

    
    ##defining network
    
    #CONV1_1
with tf.name_scope('conv1_1') as scope:
    value=weights['conv1_1_W'] # 46#
    init_weight = tf.constant_initializer(value)
    kernel = tf.get_variable(initializer=init_weight, shape=(3, 3, 3, 64),name="kernel10")
    value_bias=0.0
    init_bias = tf.constant_initializer(value_bias)
    biases = tf.get_variable(initializer=init_bias, shape=[64], trainable=True, name="bias10")
    conv1_1 = tf.nn.conv2d(input=imgs_normalized, filter=kernel,  strides=[1, 1, 1, 1], padding='SAME')
    out = tf.nn.bias_add(conv1_1, biases)
    act = tf.nn.relu(out, name=scope)

    
with tf.name_scope('conv1_2') as scope:
    value=weights['conv1_2_W']
    init_weight = tf.constant_initializer(value)
    kernel = tf.get_variable(initializer=init_weight, shape=[3, 3, 64, 64],name="kernel11")
    value_bias=0.0
    init_bias = tf.constant_initializer(value_bias)
    biases = tf.get_variable(initializer=init_bias, shape=[64], trainable=True,name="bias11")
    conv1_2 = tf.nn.conv2d(input=act, filter=kernel,  strides=[1, 1, 1, 1], padding='SAME')
    out = tf.nn.bias_add(conv1_2, biases)
    act = tf.nn.relu(out, name=scope)
    
    #CONV2_1
with tf.name_scope('pool1') as scope:
    pool1 = tf.layers.max_pooling2d(act, pool_size=(3,3), strides=(2,2), padding='same')
    
with tf.name_scope('conv2_1') as scope:
    value=weights['conv1_2_W']
    init_weight = tf.constant_initializer(value)
    kernel = tf.get_variable(initializer=init_weight, shape=[3, 3, 64, 128],name="kernel12")
    value_bias=0.0
    init_bias = tf.constant_initializer(value_bias)
    biases = tf.get_variable(initializer=init_bias, shape=[128], trainable=True,name="bias12")
    conv2_1 = tf.nn.conv2d(input=pool1, filter=kernel, strides=[1, 1, 1, 1], padding='SAME')
    out = tf.nn.bias_add(conv2_1, biases)
    act = tf.nn.relu(out, name=scope)
    
with tf.name_scope('conv2_2') as scope:
    value=weights['conv2_2_W']
    init_weight = tf.constant_initializer(value)
    kernel = tf.get_variable(initializer=init_weight, shape=[3, 3, 128, 128],name="kernel13")
    value_bias=0.0
    init_bias = tf.constant_initializer(value_bias)
    biases = tf.get_variable(initializer=init_bias, shape=[128], trainable=True,name="bias13")
    conv2_2 = tf.nn.conv2d(input=act, filter=kernel, strides=[1, 1, 1, 1], padding='SAME')
    out = tf.nn.bias_add(conv2_2, biases)
    act = tf.nn.relu(out, name=scope)
   
    
with tf.name_scope('pool2') as scope:
    pool2 = tf.layers.max_pooling2d(act, pool_size=(3,3), strides=(2,2), padding='same')

with tf.name_scope('conv3_1') as scope:
    value=weights['conv3_1_W']
    init_weight = tf.constant_initializer(value)
    kernel = tf.get_variable(initializer=init_weight,shape=[3, 3, 128, 256],name="kernel14")
    value_bias=0.0
    init_bias = tf.constant_initializer(value_bias)
    biases = tf.get_variable(initializer=init_bias, shape=[256], trainable=True,name="bias14")
    conv3_1 = tf.nn.conv2d(input=pool2, filter=kernel, strides=[1, 1, 1, 1], padding='SAME')
    out = tf.nn.bias_add(conv3_1, biases)
    act = tf.nn.relu(out, name=scope)
    
    
with tf.name_scope('conv3_2') as scope:
    value=weights['conv3_2_W']
    init_weight = tf.constant_initializer(value)
    kernel = tf.get_variable(initializer=init_weight,shape=[3, 3, 256, 256],name="kernel15")
    value_bias=0.0
    init_bias = tf.constant_initializer(value_bias)
    biases = tf.get_variable(initializer=init_bias, shape=[256], trainable=True,name="bias15")
    conv3_2 = tf.nn.conv2d(input=act, filter=kernel, strides=[1, 1, 1, 1], padding='SAME')
    out = tf.nn.bias_add(conv3_2, biases)
    act = tf.nn.relu(out, name=scope)
    
with tf.name_scope('conv3_3') as scope:
    value=weights['conv3_3_W']
    init_weight = tf.constant_initializer(value)
    kernel = tf.get_variable(initializer=init_weight,shape=[3, 3, 256, 256],name="kernel16")
    value_bias=0.0
    init_bias = tf.constant_initializer(value_bias)
    biases = tf.get_variable(initializer=init_bias, shape=[256], trainable=True,name="bias16")
    conv3_3 = tf.nn.conv2d(input=act, filter=kernel, strides=[1, 1, 1, 1], padding='SAME')
    out = tf.nn.bias_add(conv3_3, biases)
    act = tf.nn.relu(out, name=scope)
    
with tf.name_scope('pool3') as scope:
    pool3 = tf.layers.max_pooling2d(act, pool_size=(3,3), strides=(1,1), padding='same')
        
# 1x1 filter 
    
    
    
filters= tf.get_variable(shape=[1, 1, 256, 256], dtype=tf.float32, name='filter')
x = tf.nn.conv2d(pool3, filter=filters , strides=[1, 1, 1, 1], padding='SAME')

with tf.name_scope('1_1_block') as scope:
    filter_1 = tf.Variable(tf.truncated_normal([1, 1, 256, 256], name="filter1",stddev=0.01))
    block_1_1 = tf.nn.conv2d(input=x, filter=filter_1, strides=[1, 1, 1, 1], padding='SAME')
    #3x3 filter
with tf.name_scope('3_3_block') as scope:
    filter_3 = tf.Variable(tf.truncated_normal([3, 3, 256, 256], name="filter3",stddev=0.01))
    block_3_3 = tf.nn.conv2d(input=x, filter=filter_3, strides=[1, 1, 1, 1], padding='SAME')
        #block_3_3 = tf.nn.conv2d(input=x, filter=one_filter, strides=[2, 2, 2, 2], padding='SAME')                       
                               
     # average or maxpooling????                          
with tf.name_scope('max_pool') as scope:
    pooling = tf.layers.max_pooling2d(block_3_3, pool_size=(1,1), strides=(1,1), padding='same') 
         #output [batch_size, image_width, image_height, channels]
         #other form                      
      # pooling = tf.nn.avg_pool(x, ksize=[1, 3, 3, 1], strides=[1, 1, 1, 1], padding='SAME')

        
    
    ##multilevel feature map
with tf.name_scope('feature_map') as scope:
       # others fc2 , fc3 , ...three features???
    multi_feature_map = tf.concat([block_1_1, block_3_3, pooling], axis=3) 
        #pool = tf.concat([pool2,pool3,fc1], axis=3)
    
with tf.name_scope('dropout') as scope:
    tf_is_training_pl = tf.placeholder_with_default(True, shape=())
    tf_drop_out = tf.layers.dropout(multi_feature_map, rate=0.5, training=tf_is_training_pl)
    
with tf.name_scope('saliency_features') as scope:
    saliency = tf.layers.conv2d(tf_drop_out, filters=64, kernel_size=(3,3), strides=(1,1), padding="same", activation=tf.nn.relu)

        #final output is the saliency raw
with tf.name_scope('raw_saliency') as scope:    
    saliency_raw= tf.layers.conv2d(saliency, filters=1, kernel_size=(1,1), strides=(1,1), padding="same", activation=tf.nn.relu)


with tf.name_scope('loss') as scope:
	    # normalize saliency
    max_value_per_image = tf.reduce_max(saliency_raw, axis=[1,2,3], keepdims=True)
    predicted_saliency = (saliency_raw / max_value_per_image)
	    # Prediction is smaller than target, so downscale target to same size
	    #target_shape = (56,56)
    target_shape = predicted_saliency.shape[1:3]
    target_downscaled = tf.image.resize_images(fixations_normalized, target_shape)
	    # Loss function from Cornia et al. (2016) [with higher weight for salient pixels]
    alpha = 1.01 
    weights = 0.001/(alpha - target_downscaled)
    loss = tf.losses.mean_squared_error(labels=target_downscaled, predictions=predicted_saliency, weights=weights)
    with tf.variable_scope("Momentum", reuse=tf.AUTO_REUSE):
        minimize_op = tf.train.MomentumOptimizer(learning_rate=0.01, momentum=0.9, use_nesterov=True, name="Momentum").minimize(loss)
    #adam_op=tf.train.AdamOptimizer(learning_rate=0.01).minimize(loss)
    
loss_summary = tf.summary.scalar(name="loss", tensor=loss)

input_raw=tf.summary.image("train image data", predicted_saliency, max_outputs=320)
image_raw.append(input_raw)

saver = tf.train.Saver()
    
with tf.Session() as sess:
    writer = tf.summary.FileWriter(logdir="./", graph=sess.graph)
  #  graph = tf.get_default_graph() # sess.graph
    sess.run(tf.global_variables_initializer())
    #saver.restore(sess, './my-model-9')
    gen = data_generator(train_imgs, train_fixations)

    
    for b in range(num_batches):
        batch_imgs, batch_fixations = get_batch_from_generator(gen, batch_size)
        idx = np.random.choice(train_imgs.shape[0], batch_size, replace=False) # sample random indices
        
        _, l, batch_loss,iraw = sess.run([minimize_op, loss_summary, loss, image_raw[0]], 
        feed_dict={ims_holder: train_imgs[idx,...], targs_holder: train_fixations[idx]})

        writer.add_summary(l, global_step=b)
        writer.add_summary(iraw, global_step=b)
        print('Batch {:d} done: batch loss {:f}'.format(b, batch_loss))
        save_path = saver.save(sess, "./my-model", global_step=b)

  

    # we could save at end of training, for example
save_path = saver.save(sess, "./my-model", global_step=b)
 


